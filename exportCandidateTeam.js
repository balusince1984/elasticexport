const fs = require('fs');

module.exports = function (app, utilities, env, skipToPage) {

    const sendData = utilities.sendData;
    const getData = utilities.getData;
    const sendData1 = utilities.sendData1;
    var stream = fs.createWriteStream((new Date()).getTime() + "candidatescript.cql");
    var jump = skipToPage;

    var context = {
        _scroll_id: null,
        _job_scroll_id: null,
        index: 0,
        jobIndex : 0,
        jobsResponse: null,
        jobCount:0,
    }

    app.get("/api/start/exportcandidates", function (req, res) {
        if (context.running == 1) {
            res.send("Already Running");
            return
        };
        start();
    });

    function start(){
        console.log("Process Started");
		context.running = 1;
        context.index = 0;
        context.stop = 0;        
        createJobScrollToken();
	}

    app.get("/api/stop/exportcandidates", function (req, res) {
        context.running = 0;
        context.stop = 1;
        res.send("Gracefull shutdown");
    });    

    function createJobScrollToken() {
        sendData({
            "size": 10000,
            "query": {
                "match_all": {}
            }
        }, utilities.getJobIndex() + "job/job/_search?scroll=1m", "POST", {
                scroll: "1m"
            }).then(function (response) {
                context._job_scroll_id = response._scroll_id;
                if(jump == 0){
                    context.jobsResponse = response;
                    context.jobIndex = 0;
                    nextJob();
                }else{
                    jump = jump - 1;
                    scrollToNextJobs();
                }
            }).catch(function (err) {
                console.log(err);
            });
    }

    function scrollToNextJobs(){
        console.log("Scrolling To Next Jobs Page. Exit the process");
        sendData({
            "scroll": "1m",
            "scroll_id": context._job_scroll_id
        }, utilities.getJobIndex() + "/_search/scroll", "POST").then(function (response) {
            if (response.hits.hits.length > 0) {
                context._job_scroll_id = response._scroll_id;
                if(jump == 0){
                    context.jobsResponse = response;
                    context.jobIndex = 0;
                    nextJob();
                }else{
                    jump = jump - 1;
                    scrollToNextJobs();
                }
            }else{
                console.log("Process completed");
            }
        }).catch(function (err) {
            console.log(err);
        });
    }

    function nextJob(){        
        if (context.jobsResponse.hits.hits[context.jobIndex]) {
            context.jobCount = context.jobCount + 1
            console.log("Next Job " + (context.jobCount));
            context.job = context.jobsResponse.hits.hits[context.jobIndex]["_source"];
            createCandidateScrollToken();
        }else{
            //scrollToNextJobs();
		console.log("Exit the process");
        }        
    }

    function createCandidateScrollToken() {
        sendData({
            "size": 10000,
            "_source": {
                "include": ["id", "jobId", "team.*", "recruiterId", "isJobCandidateMapping"]
            },            
            "query": {
                "bool": {
                    "must": [{
                        "term": {
                            "jobId": context.job.id
                        }
                    }]
                }
            }              
        }, utilities.getCandidateIndex() + "jobcandidateinteraction/jobcandidateinteraction/_search?scroll=1m", "POST", {
                scroll: "1m"
            }).then(function (response) {
                writeToFile(response);
            }).catch(function (err) {
                console.log(err);
            });
    }

    function writeToFile(response) {
        if (response.hits.hits.length > 0) {
            context.index = 0;
            context._scroll_id = response._scroll_id;
            attachJobAndWriteToFile(response);
        }else{
            context.jobIndex = context.jobIndex + 1;
            nextJob();
        }
    }

    function attachJobAndWriteToFile(response) {
        if (response.hits.hits[context.index]) {
            var candidate = response.hits.hits[context.index]["_source"]; 
            var job = context.job;       
            var data = [];                
            if (job.accountManagerName)
                data.push("jobAccountManagerName='" + escapeQuote(job.accountManagerName) + "'");
            if (job.accountManagerEmail)
                data.push("jobAccountManagerEmail='" + escapeQuote(job.accountManagerEmail) + "'");
            if (job.clientRecruiterName)
                data.push("jobClientRecruiterName='" + escapeQuote(job.clientRecruiterName) + "'");
            if (job.clientRecruiter)
                data.push("jobClientRecruiterEmail='" + escapeQuote(job.clientRecruiter) + "'");
            if (job.jobOwner)
                data.push("jobPostedByRecruiterName='" + escapeQuote(job.jobOwner) + "'");
            if (job.recruiterId)
                data.push("jobPostedByRecruiterEmail='" + escapeQuote(job.recruiterId) + "'");
            if (candidate.team)
                data.push("teamId=" + candidate.team.id);
            if (job.team)
                data.push("jobPostedByTeamId=" + job.team.id);
            if (job.assignedTo && job.assignedTo.length > 0) {
                for (i in job.assignedTo) {
                    var wa = job.assignedTo[i];
                    if (wa.recruiterId == candidate.recruiterId) {
                        data.push("jobWorkAssignmentId=" + wa.id);
                        break;
                    }
                }
            }
            if (candidate.isJobCandidateMapping) {
                stream.write("UPDATE job_candidate_mapping SET " + data.join(",") + " WHERE id = " + candidate.id + ";\n");
            } else {
                stream.write("UPDATE jobcandidateinteraction SET " + data.join(",") + " WHERE id = " + candidate.id + ";\n");
            }
            context.index = context.index + 1;
            attachJobAndWriteToFile(response);
        } else {
            scrollToCandidateNextPage();
        }
    }

    function scrollToCandidateNextPage() {
        //console.log("Scrolling To Next Candidates Page");
        sendData({
            "scroll": "1m",
            "scroll_id": context._scroll_id
        }, utilities.getCandidateIndex() + "/_search/scroll", "POST").then(function (response) {
            writeToFile(response);
        }).catch(function (err) {
            console.log(err);
        });;
    }

    function eq(r){
        return r.replace(/'/g, "''");
    }

    setTimeout(start,5000);

}
