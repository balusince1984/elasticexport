const fs = require('fs');
const uuidv1 = require('uuid/v1');

module.exports = function (app, utilities, env) {

    const sendData = utilities.sendData;
    const getData = utilities.getData;
    const sendData1 = utilities.sendData1;
    fs.existsSync("email_track.cql") && fs.unlinkSync("email_track.cql")
    var stream = fs.createWriteStream("email_track.cql");


    var context = {
        _scroll_id: null
    }

    function start() {
        console.log("Process Started");
        context.running = 1;
        context.index = 0;
        context.stop = 0;
        createScrollToken();
    }

    function processESData() {
        sendData({
            "scroll": "1m",
            "scroll_id": context._scroll_id
        }, utilities.getJobIndex() + "/_search/scroll", "POST").then(function (response) {
            writeToFile(response);
        }).catch(function (err) {
            console.log(err);
        });;
    }

    function createScrollToken() {
        sendData({
            "size": 10000,
            "query": {
                "match_all": {}
            },
            "sort": [
                {
                    "timestamp": {
                        "order": "asc"
                    }
                }
            ]
        }, utilities.getJobIndex() + "emailevents/emailevents/_search?scroll=1m", "POST", {
                scroll: "1m"
            }).then(function (response) {
                writeToFile(response);
            }).catch(function (err) {
                console.log(err);
            });
    }

    function writeToFile(response) {
        if (response.hits.hits.length > 0) {
            context._scroll_id = response._scroll_id;
            for (var i in response.hits.hits) {
                var email = response.hits.hits[i]["_source"];
                var d = new Date(0);
                d.setUTCSeconds(email.timestamp);
                var jsonString = JSON.stringify(email);
                var emailTo = email.message.headers.to || email.recipient;
                var event = email["event"] ? email["event"] : '';
                var logLevel = email["log-level"] ? email["log-level"] : '';
                var reason = email["reason"] ? email["reason"] : '';
                var severity = email["severity"] ? email["severity"] : '';
                console.log(emailTo + "," + event + "," + logLevel + "," + reason + "," + severity);
                stream.write("INSERT INTO email_tracking(email,event,logLevel,reason,severity,updatedOn) values('" + emailTo + "','" + event + "','" + logLevel + "','" + reason + "','" + severity + "','" + d.toISOString() + "');\n");
                stream.write("INSERT INTO email_tracking_log(email,createdOn,id,event,logLevel,severity,reason,eventData) values('" + emailTo + "','" + d.toISOString() + "'," + uuidv1() + ",'" + event + "','" + logLevel + "','" + severity + "','" + reason + "','" + jsonString + "');\n");
            }
            //processESData();
        } else {
            console.log("Process completed");
        }
    }

    setTimeout(start, 5000);

}
