const fs = require('fs');

module.exports = function (app, utilities, env) {

    const sendData = utilities.sendData;
    const getData = utilities.getData;
    const sendData1 = utilities.sendData1;
    fs.existsSync("job_workassignment.cql") && fs.unlinkSync("job_workassignment.cql")
    var stream = fs.createWriteStream("job_workassignment.cql");
   

    var context = {
        _scroll_id: null
    }

    app.get("/api/start/exportjob", function (req, res) {
        if (context.running == 1) {
            res.send("Already Running");
            return
        };
        //context.running = 1;
        //context.index = 0;
        //context.stop = 0;
        //res.send("Exporting");
        //createScrollToken();
	    start();
    });

	function start(){
        console.log("Process Started");
		context.running = 1;
        context.index = 0;
        context.stop = 0;
        //res.send("Exporting");
        createScrollToken();
	}

    app.get("/api/stop/exportjob", function (req, res) {
        context.running = 0;
        context.stop = 1;
        res.send("Gracefull shutdown");
    });

    function processESData() {
        sendData({
            "scroll": "1m",
            "scroll_id": context._scroll_id
        }, utilities.getJobIndex() + "/_search/scroll", "POST").then(function (response) {
            writeToFile(response);
        }).catch(function (err) {
            console.log(err);
        });;
    }

    function createScrollToken() {
        sendData({
            "size": 10000,
            "query": {
                "match_all": {}
            }
        }, utilities.getJobIndex() + "job/job/_search?scroll=1m", "POST", {
                scroll: "1m"
            }).then(function (response) {
                writeToFile(response);
            }).catch(function (err) {
                console.log(err);
            });
    }

    function writeToFile(response) {
        if (response.hits.hits.length > 0) {
            context._scroll_id = response._scroll_id;
            for (var i in response.hits.hits) {
                var job = response.hits.hits[i]["_source"];
                if (job.team) {
                    stream.write("UPDATE job SET jobPostedByTeamId = " + job.team.id + " WHERE id=" + job.id + ";\n");
                }
                if (job.assignedTo && job.assignedTo.length > 0) {
                    for (var j in job.assignedTo) {
                        var wa = job.assignedTo[j];
                        if (wa.team) {
                            stream.write("UPDATE workAssignment SET assignedToTeamId = " + wa.team.id + " WHERE id=" + wa.id + ";\n");
                            stream.write("UPDATE workAssignment_by_jobId SET assignedToTeamId = " + wa.team.id + " WHERE jobid='" + job.id + "' AND id = " + wa.id + ";\n");
                            stream.write("UPDATE workAssignment_by_recruiterId SET assignedToTeamId = " + wa.team.id + " WHERE recruiterid='" + wa.recruiterId + "' AND isclosed = " + wa.isClosed + " and isaccepted = " + wa.isAccepted + " AND id = " + wa.id + ";\n");
                        }
                    }
                }
            }
            processESData();
        }else{
            console.log("Process completed");
        }
    }

	setTimeout(start,5000);

}
