const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
//Initiallising node modules
const express = require("express");
const bodyParser = require("body-parser");
const utilities = require('./utilities');
//Initalise utilities
utilities.init(env);

const rp = require('request-promise');
const querystring = require('querystring');
const errorhandler = require('errorhandler')
const app = express();


//node server.js 7001 1 2
// Body Parser Middleware
app.use(bodyParser.json());

//CORS Middleware
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

var port = process.argv[2];
var type = process.argv[3];


//Setting up server
var server = app.listen(port || 7001, function () {
    var port = server.address().port;
    console.log("App now running on port :: " + port);
});

if (type == "1")
    require('./exportJobTeam')(app, utilities, env);
if (type == "2"){
    var skipToPage = process.argv[4];
    require('./exportCandidateTeam')(app, utilities, env, skipToPage);
}
if (type == "3"){
    var skipToPage = process.argv[4];
    require('./updateCandidates')(app, utilities, env, skipToPage);
}
if (type == "4"){
    var skipToPage = process.argv[4];
    require('./synccandidatemappings')(app, utilities, env, skipToPage);
}
if (type == "5"){
    var skipToPage = process.argv[4];
    require('./synccandidatemappingslinkedin')(app, utilities, env, skipToPage);
}
if (type == "6"){
    var skipToPage = process.argv[4];
    require('./updatelastmapdatelastuploaddate')(app, utilities, env, skipToPage);
}
if (type == "7"){
    var skipToPage = process.argv[4];
    require('./synccandidatesubmissions')(app, utilities, env, skipToPage);
}

if (type == "8"){
    var skipToPage = process.argv[4];
    require('./exportjobdetails')(app, utilities, env, skipToPage);
}

if (type == "9"){
    var skipToPage = process.argv[4];
    require('./exportEmailTracking')(app, utilities, env, skipToPage);
}

if (type == "10"){
    var skipToPage = process.argv[4];
    require('./tempscript')(app, utilities, env, skipToPage);
}

//
//require('./candidateMappings')(app, utilities);
//require('./jobteam')(app, utilities);
//require('./recruierteam')(app, utilities);
///
//
//
//
