var moment = require('moment');
const fs = require('fs');

module.exports = function (app, utilities, env) {

    const logger = utilities.logger;
    const sendData = utilities.sendData;
    const getData = utilities.getData;
    var count = 0;
    var skip = -1;


    var context = {
        _scroll_id: null,
        mappingIndex: -1,
        response: null
    }

    function start() {
        logger.info("Process Started");
        context.running = 1;
        context.index = 0;
        context.stop = 0;
        createScrollToken();
    }

    function createScrollToken() {
        sendData({
            "size": 5000,
            "query": {
                "match_all": {}
            }, "_source": {
                "include": ["id", "jobId", "createdOn", "candidateId", "email", "isJobCandidateMapping", "mappedJobLocation", "contactActions", "preferredLocations.country", "preferredLocations.continent", "currentLocation.country", "currentLocation.continent", "alternateEmails", "phoneNumber", "linkedInURL", "resumeLocation", "alternatePhoneNumbers", "mappedCandidateResumes.resumeLocation", "mappedCandidateResumes.resumeUploadedOn"]
            }
        }, utilities.getCandidateIndex() + "jobcandidateinteraction/jobcandidateinteraction/_search?scroll=1m", "POST", {
                scroll: "1m"
            }).then(function (response) {
                context.response = response;
                context._scroll_id = response._scroll_id;
                skip = skip - 1;
                if (skip <= 0)
                    nextRecord();
                else {
                    logger.info("Skipping %s", skip)
                    processESData();
                }
            }).catch(function (err) {
                logger.error(err);
            });
    }

    function processESData() {
        logger.info("Parsed records %s", count);
        sendData({
            "scroll": "2m",
            "scroll_id": context._scroll_id
        }, utilities.getCandidateIndex() + "/_search/scroll", "POST").then(function (response) {
            try {
                context.response = response;
                context.mappingIndex = -1;
                context._scroll_id = response._scroll_id;
                if (context.response.hits.hits.length > 0) {
                    skip = skip - 1;
                    if (skip <= 0)
                        nextRecord();
                    else {
                        logger.info("Skipping %s", skip)
                        processESData();
                    }
                } else {
                    logger.info("Exit the process");
                }
            } catch (err) {
                logger.error(err);
            }
        }).catch(function (err) {
            logger.error(err);
        });
    }

    function syncCandidateMappings() {
        var cand = context.response.hits.hits[context.mappingIndex]["_source"];
        var obj = null;
        var query = {
            "query": {
                "bool": {
                    "must": [{
                        "term": {
                            "id": cand.candidateId
                        }
                    }]
                }
            }
        };
        sendData(query, utilities.getCandidateIndex() + "/candidateaggregator/_search", "POST").then(function (response) {
            if (response.hits.hits.length > 0) {
                obj = response.hits.hits[0]["_source"];
            } else {
                obj = {
                    "id": cand.candidateId,
                    "location": [],
                    "email": [],
                    "linkedIn": [],
                    "phoneNumber": [],
                    "cv": [],
                    "cvCount": 0,
                    "mapping": [],
                    "contact": [],
                    "mappingCount": 0,
                    "lastMappingDate": "",
                    "lastUploadDate": "",
                    "firstMappingDate": "",
                    "firstUploadDate": "",
                    "lastContactDate": "",
                    "firstContactDate": "",
                }
            }
            count++
            if (addMapping(obj.mapping, {
                "createdOn": formatDate(cand.createdOn),
                "value": cand.jobId && cand.jobId.trim().length > 0 ? cand.jobId : "IMPORT"
            })) {
                try {
                    if(cand.jobId && cand.jobId.trim().length > 0)
                        obj.mappingCount = obj.mappingCount + 1;                    
                    try {
                        var contactActions = JSON.parse(cand.contactActions);
                        if (contactActions && contactActions.length > 0)
                            for (var c = 0; c < contactActions.length; c++) {
                                var ca = contactActions[c];
                                obj.mapping.push({
                                    "createdOn": formatDate(ca.createdOn),
                                    "value": "CONTACTACTION"
                                });
                                obj.contact.push({
                                    "createdOn": formatDate(ca.createdOn),
                                    "value": "CONTACTACTION"
                                });
                            }
                    } catch (err) {
                        logger.error("Invalid contact actions object %j %s ", err, cand.contactActions);
                    }
                    if (cand.email && cand.email.trim().length > 0)
                        addListToSet(obj.email, [cand.email.trim()]);
                    if (cand.alternateEmails && cand.alternateEmails.length > 0)
                        addListToSet(obj.email, cand.alternateEmails);
                    if (cand.linkedInURL && cand.linkedInURL.trim().length > 0)
                        addListToSet(obj.linkedIn, [cand.linkedInURL.trim()]);
                    if (cand.phoneNumber && cand.phoneNumber.trim().length > 0)
                        addListToSet(obj.phoneNumber, [cand.phoneNumber.trim()]);
                    if (cand.alternatePhoneNumbers && cand.alternatePhoneNumbers.length > 0)
                        addListToSet(obj.phoneNumber, cand.alternatePhoneNumbers);
                    if (cand.mappedCandidateResumes && cand.mappedCandidateResumes.length > 0)
                        for (var r in cand.mappedCandidateResumes) {
                            var resume = cand.mappedCandidateResumes[r];
                            addItemToSet(obj.cv, {
                                "createdOn": formatDate(resume.resumeUploadedOn),
                                "value": resume.resumeLocation
                            });
                        }
                    if (cand.resumeLocation && cand.resumeLocation.trim().length > 0)
                        addItemToSet(obj.cv, {
                            "createdOn": formatDate(cand.createdOn),
                            "value": cand.resumeLocation.trim()
                        });

                    if (cand.preferredLocations && cand.preferredLocations.length > 0) {
                        for (var p = 0; p < cand.preferredLocations.length; p++) {
                            var location = cand.preferredLocations[p];
                            if ((location.country && location.country.length > 0) || (location.continent && location.continent.length > 0))
                                addLocation(obj.location, {
                                    "country": location.country ? location.country.trim() : "",
                                    "continent": (location.country.trim().toLowerCase() == "united states" || location.country.trim().toLowerCase().indexOf("usa") >= 0 || location.country.trim().toLowerCase().indexOf("united states") >= 0) ? "North America" : "Europe"
                                });
                        }
                    }

                    if (cand.currentLocation && ((cand.currentLocation.country && cand.currentLocation.country.length > 0) || (cand.currentLocation.continent && cand.currentLocation.continent.length > 0)))
                        addLocation(obj.location, {
                            "country": cand.currentLocation.country ? cand.currentLocation.country.trim() : "",
                            "continent": cand.currentLocation.continent ? cand.currentLocation.continent.trim() : ""
                        });
                    else if (cand.mappedJobLocation && cand.mappedJobLocation.trim().length > 0) {
                        //logger.info("No job location %s ", cand.mappedJobLocation);
                        //logger.info("Pre job location %j ", obj.location);
                        if (cand.mappedJobLocation.indexOf(",") >= 0) {
                            var location = cand.mappedJobLocation.split(",");
                            var country = location[location.length - 1];
                            addLocation(obj.location, {
                                "country": country ? country.trim() : "",
                                "continent": (country.trim().toLowerCase() == "united states" || country.trim().toLowerCase().indexOf("usa") >= 0 || country.trim().toLowerCase().indexOf("united states") >= 0) ? "North America" : "Europe"
                            });
                        } else {
                            addLocation(obj.location, {
                                "country": "",
                                "continent": cand.mappedJobLocation.trim()
                            });
                        }
                        //logger.info("Parsed job location %j ", obj.location);
                    }
                    obj.cvCount = obj.cv.length;
                    obj.lastMappingDate = getLatestDate(obj.mapping);
                    obj.lastContactDate = getLatestDate(obj.contact);
                    obj.lastUploadDate = getLatestDate(obj.cv);
                    obj.firstMappingDate = getFirstDate(obj.mapping);
                    obj.firstContactDate = getFirstDate(obj.contact);
                    obj.firstUploadDate = getFirstDate(obj.cv);
                    sendData(obj, utilities.getCandidateIndex() + "/candidateaggregator/candidateaggregator/" + cand.candidateId, "POST").then(function (response) {
                        nextRecord();
                    }).catch(function (err) {
                        logger.error(err);
                        nextRecord();
                    });
                } catch (err) {
                    logger.error(err);
                }
            } else
                nextRecord();
        }).catch(function (err) {
            logger.error(err);
        });
    }

    function nextRecord() {
        try {
            context.mappingIndex = context.mappingIndex + 1;
            if (context.response.hits.hits[context.mappingIndex]) {
                syncCandidateMappings();
            } else {
                processESData();
            }
        } catch (err) {
            logger.error(err);
        }
    }

    function addListToSet(l, s) {
        for (var k in s) {
            if (l.indexOf(s[k]) < 0) l.push(s[k].trim());
        }
    }

    //2018-01-31T16:32:00.020Z

    //2018-01-30T16:17:16Z
    //2018-01-30T16:17:10.749Z
    function formatDate(x) {
        try {
            if (x.lastIndexOf(".") < 0) {
                var y = x.substring(x.lastIndexOf(":") + 1);
                var z = y;
                if (y.length == 3) {
                    y = "00.0" + y;
                    x = x.replace(z, y);
                } else if (y.length == 2) {
                    y = "00.00" + y;
                    x = x.replace(z, y);
                }
            }
            return x;
        } catch (err) {
            logger.error(err);
        }
    }

    function addItemToSet(l, s) {
        try {
            for (var k in l) {
                if (l[k].value == s.value) {
                    return;
                }
            }
            l.push(s);

        } catch (err) {
            logger.error(err);
        }
    }

    function addLocation(l, i) {
        try {
            for (var k in l) {
                if (l[k].country == i.country && l[k].continent == i.continent) {
                    return;
                }
            }
            l.push(i);
        } catch (err) {
            logger.error(err);
        }
    }

    function addMapping(l, i) {
        try {
            for (var k in l) {
                if (l[k].value == i.value && l[k].createdOn == i.createdOn) {
                    return false;
                }
            }
            l.push(i);
            return true;
        } catch (err) {
            logger.error(err);
        }
    }

    function getLatestDate(dates) {
        var lastDate = null;
        for (d in dates) {
            if (lastDate == null) {
                lastDate = dates[d].createdOn;
                continue;
            }
            if (!moment(dates[d].createdOn).isBefore(lastDate)) {
                lastDate = dates[d].createdOn;
            }
        }
        //if (dates.length > 1)
            //logger.info("Last date among %j is %s", dates, lastDate);
        return lastDate;
    }

    function getFirstDate(dates) {
        var firstDate = null;
        for (d in dates) {
            if (firstDate == null) {
                firstDate = dates[d].createdOn;
                continue;
            }
            if (moment(dates[d].createdOn).isBefore(firstDate)) {
                firstDate = dates[d].createdOn;
            }
        }
        //if (dates.length > 1)
            //logger.info("First date among %j is %s", dates, firstDate);
        return firstDate;
    }

    setTimeout(start, 2000);

}
