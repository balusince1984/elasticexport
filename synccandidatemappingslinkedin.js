const fs = require('fs');

module.exports = function (app, utilities, env) {

    const sendData = utilities.sendData;
    const getData = utilities.getData;
    var count = 0;
    var skip = 0;


    var context = {
        _scroll_id: null,
        mappingIndex: -1,
        response: null
    }

    function start() {
        console.log("Process Started");
        context.running = 1;
        context.index = 0;
        context.stop = 0;
        createScrollToken();
    }

    function createScrollToken() {
        sendData({
            "size": 5000,
            "query": {
                "bool": {
                  "must": [
                      { "exists": {"field": "linkedInURL"}}
                  ]
                }
              }, "_source": {
                "include": ["id", "jobId", "createdOn", "candidateId", "email", "isJobCandidateMapping", "currentLocation.country", "currentLocation.continent", "alternateEmails", "phoneNumber", "linkedInURL", "resumeLocation", "alternatePhoneNumbers", "mappedCandidateResumes.resumeLocation", "mappedCandidateResumes.resumeUploadedOn"]
            }
        }, utilities.getCandidateIndex() + "jobcandidateinteraction/jobcandidateinteraction/_search?scroll=1m", "POST", {
                scroll: "1m"
            }).then(function (response) {
                context.response = response;
                context._scroll_id = response._scroll_id;
                skip = skip - 1;
                if(skip <= 0)
                    nextRecord();
                else{
                    console.log("Skipping " + skip)
                    processESData();
                }
            }).catch(function (err) {
                console.log(err);
            });
    }

    function processESData() {
        sendData({
            "scroll": "1m",
            "scroll_id": context._scroll_id
        }, utilities.getCandidateIndex() + "/_search/scroll", "POST").then(function (response) {
            context.response = response;
            context.mappingIndex = -1;
            context._scroll_id = response._scroll_id;
            if (context.response.hits.hits.length > 0) {
                skip = skip - 1;
                if(skip <= 0)
                    nextRecord();
                else{
                    console.log("Skipping " + skip)
                    processESData();
                }
            } else {
                console.log("Exit the process");
            }
        }).catch(function (err) {
            console.log(err);
        });
    }

    function syncCandidateMappings() {
        var cand = context.response.hits.hits[context.mappingIndex]["_source"];
        var obj = null;
        var query = {
            "query": {
                "bool": {
                    "must": [{
                        "term": {
                            "id": cand.candidateId
                        }
                    }]
                }
            }
        };
        sendData(query, utilities.getCandidateIndex() + "/candidateaggregator/_search", "POST").then(function (response) {
            if (response.hits.hits.length > 0) {
                obj = response.hits.hits[0]["_source"];
            } else {
                obj = {
                    "id": cand.candidateId,
                    "location": [],
                    "email": [],
                    "linkedIn": [],
                    "phoneNumber": [],
                    "cv": [],
                    "cvCount": 0,
                    "mapping": [],
                    "mappingCount": 0

                }
            }
            if (cand.email && cand.email.trim().length > 0)
                addListToSet(obj.email, [cand.email.trim()]);
            if (cand.alternateEmails && cand.alternateEmails.length > 0)
                addListToSet(obj.email, cand.alternateEmails);
            if (cand.linkedInURL && cand.linkedInURL.trim().length > 0)
                addListToSet(obj.linkedIn, [cand.linkedInURL.trim()]);
            if (cand.phoneNumber && cand.phoneNumber.trim().length > 0)
                addListToSet(obj.phoneNumber, [cand.phoneNumber.trim()]);
            if (cand.alternatePhoneNumbers && cand.alternatePhoneNumbers.length > 0)
                addListToSet(obj.phoneNumber, cand.alternatePhoneNumbers);
            if (cand.mappedCandidateResumes && cand.mappedCandidateResumes.length > 0)
                for (var r in cand.mappedCandidateResumes) {
                    var resume = cand.mappedCandidateResumes[r];
                    addItemToSet(obj.cv, {
                        "createdOn": formatDate(resume.resumeUploadedOn),
                        "value": resume.resumeLocation
                    });
                }
            if (cand.resumeLocation && cand.resumeLocation.trim().length > 0)
                addItemToSet(obj.cv, {
                    "createdOn": formatDate(cand.createdOn),
                    "value": cand.resumeLocation
                });
            if (cand.currentLocation)
                addItemToSet(obj.location, {
                    "country": cand.currentLocation.country,
                    "continent": cand.currentLocation.continent
                });
            if (cand.jobId && cand.jobId.trim().length > 0) {
                obj.mapping.push({
                    "createdOn": formatDate(cand.createdOn),
                    "value": cand.jobId
                });
                obj.mappingCount = obj.mappingCount + 1;
            }
            obj.cvCount = obj.cv.length;
            console.log(count++);
            sendData(obj, utilities.getCandidateIndex() + "/candidateaggregator/candidateaggregator/" + cand.candidateId, "POST").then(function (response) {
                nextRecord();
            }).catch(function (err) {
                console.log(err);
                nextRecord();
            });;

        }).catch(function (err) {
            console.log(err);
        });
    }

    function nextRecord() {
        context.mappingIndex = context.mappingIndex + 1;
        if (context.response.hits.hits[context.mappingIndex]) {
            syncCandidateMappings();
        } else {
            processESData();
        }
    }

    function addListToSet(l, s) {
        for (var k in s) {
            if (l.indexOf(s[k]) < 0) l.push(s[k]);
        }
    }

    //2018-01-30T16:17:16Z
    //2018-01-30T16:17:10.749Z
    function formatDate(x) {
        var y = x.substring(x.lastIndexOf(":") + 1);
        var z = y;
        if (y.length == 3) {
            y = "0" + y;
            x = x.replace(z, y);
        } else if (y.length == 2) {
            y = "00" + y;
            x = x.replace(z, y);
        }        
        return x;
    }

    function addItemToSet(l, s) {
        for (var k in l) {
            if (l[k].value == s.value) {
                return;
            }
        }
        l.push(s);
    }

    function addLocation(l, i) {
        for (var k in l) {
            if (l[k].country == i.country && l[k].continent == i.continent) {
                return;
            }
        }
        l.push(i);
    }

    setTimeout(start, 2000);

}
