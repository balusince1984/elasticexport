var moment = require('moment');
const fs = require('fs');

module.exports = function (app, utilities, env) {

    const logger = utilities.logger;
    const sendData = utilities.sendData;
    const getData = utilities.getData;
    var stream = fs.createWriteStream("export.csv");
    var count = 0;
    var skip = -1;


    var context = {
        _scroll_id: null,
        mappingIndex: -1,
        response: null
    }

    function start() {
        logger.info("Process Started");
        context.running = 1;
        context.index = 0;
        context.stop = 0;
        createScrollToken();
    }

    function createScrollToken() {
        sendData({
            "size": 10000,
            "_source": { "include" : ["event","message.headers.to", "recipient"] }
        }, utilities.getCandidateIndex() + "emailevents/emailevents/_search?scroll=1m", "POST", {
                scroll: "1m"
            }).then(function (response) {
                context.response = response;
                exportToCSV1(response);
            }).catch(function (err) {
                logger.error(err);
            });
    }

    function exportToCSV(response){
        for (var i in context.response.hits.hits) {
            var record = context.response.hits.hits[i]["_source"];
            var array = [];
            array.push([record.name ? record.name: ""]);
            array.push([record.phoneNumber ? record.phoneNumber : ""]);
            array.push([record.email ? record.email : ""]);
            array.push([record.isJobCandidateMapping ? "false" : "true"]);
            array.push([record.applicationStage ? record.applicationStage : ""]);
            array.push([record.applicationState ? record.applicationState : ""]);
            array.push([record.createdOn ? record.createdOn : ""]);
            array.push([record.mappedJobTitle ? record.mappedJobTitle : ""]);
            array.push([record.companyName ? record.companyName : ""]);
            stream.write(array.join(",") + "\n");
        }
	console.log("Finished");
    }

	function exportToCSV1(response){
        for (var i in context.response.hits.hits) {
            var record = context.response.hits.hits[i]["_source"];
            var array = [];         
		if(record.message && record.message.headers && record.message.headers.to && record.message.headers.to.length > 0){
			
		}else{
			console.log(record);
		}
            array.push([(record.message && record.message.headers && record.message.headers.to && record.message.headers.to.length > 0) ? record.message.headers.to : record.recipient]);
            array.push([record.event ? record.event : ""]);
            stream.write(array.join(",") + "\n");
        }
	console.log("Finished");
    }

    setTimeout(start, 2000);

}
