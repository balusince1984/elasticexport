const fs = require('fs');

module.exports = function (app, utilities, env, skipToPage) {

    const logger = utilities.logger;
    const sendData = utilities.sendData;
    var jump = skipToPage;

    var context = {
        _job_scroll_id: null,
        workassignemntIndex: 0,
        jobIndex: -1,
        jobsResponse: null,
        jobCount: 0,
    }

    function start() {
        console.log("Process Started with skip to page " + skipToPage + ", " + jump);
        context.running = 1;
        context.index = 0;
        context.stop = 0;
        createJobScrollToken();
    }

    function createJobScrollToken() {
        sendData({
            "size": 10000,
            "query": {
                "match_all": {}
            }
        }, utilities.getJobIndex() + "job/job/_search?scroll=1m", "POST", {
                scroll: "1m"
            }).then(function (response) {
                context._job_scroll_id = response._scroll_id;
                if (jump == 0) {
                    context.jobsResponse = response;
                    context.jobIndex = -1;
                    nextJob();
                } else {
                    console.log("skipping to page " + (jump - 1));
                    jump = jump - 1;
                    scrollToNextJobs();
                }
            }).catch(function (err) {
                console.log(err);
            });
    }

    function scrollToNextJobs() {
        sendData({
            "scroll": "1m",
            "scroll_id": context._job_scroll_id
        }, utilities.getJobIndex() + "_search/scroll?scroll=1m", "POST", {
                scroll: "1m"
            }).then(function (response) {
                context._job_scroll_id = response._scroll_id;
                if (jump == 0) {
                    context.jobsResponse = response;
                    context.jobIndex = 0;
                    nextJob();
                } else {
                    console.log("skipping to page " + (jump - 1));
                    jump = jump - 1;
                    scrollToNextJobs();
                }
            }).catch(function (err) {
                console.log(err);
            });
    }

    function nextJob() {
        context.jobIndex = context.jobIndex + 1;
        if (context.jobsResponse.hits.hits[context.jobIndex]) {
            context.jobCount = context.jobCount + 1;
            logger.info("Next Job %s" , (context.jobCount));
            context.job = context.jobsResponse.hits.hits[context.jobIndex]["_source"];
            tagJobInformationToCandidate();
            //tagWorkAssignmentInformationToCandidate();
        } else {
            console.log("Exit the process");
        }
    }

    function tagJobInformationToCandidate() {
        var job = context.job;
        var query = {
            "query": {
                "bool": {
                    "must": [{
                        "term": {
                            "jobId": job.id
                        }
                    }]
                }
            },
            "script": {
                "inline": (job.accountManagerName && job.accountManagerName.length > 0 ? "ctx._source.jobAccountManagerName='" + eq(job.accountManagerName.toLowerCase().trim()) + "';" : "") +
                    (job.accountManagerEmail && job.accountManagerEmail.length > 0 ? "ctx._source.jobAccountManagerEmail='" + eq(job.accountManagerEmail.toLowerCase().trim()) + "';" : "") +
                    (job.clientRecruiterName && job.clientRecruiterName.length > 0 ? "ctx._source.jobClientRecruiterName='" + eq(job.clientRecruiterName.toLowerCase().trim()) + "';" : "") +
                    (job.clientRecruiter && job.clientRecruiter.length > 0 ? "ctx._source.jobClientRecruiterEmail='" + eq(job.clientRecruiter.toLowerCase().trim()) + "';" : "") +
                    (job.jobOwner && job.jobOwner.length > 0 ? "ctx._source.jobPostedByRecruiterName='" + eq(job.jobOwner.toLowerCase().trim()) + "';" : "") +
                    (job.recruiterId && job.recruiterId.length > 0 ? "ctx._source.jobPostedByRecruiterEmail='" + eq(job.recruiterId.toLowerCase().trim()) + "';" : "") +
                    (job.tags && job.tags.length > 0 ? "ctx._source.jobTags=[" + job.tags.map(function(tag){return "'" + eq(tag) + "'";}).join(",") + "];" : "") +
                    (job.skills && job.skills.length > 0 ? "ctx._source.jobSkills=[" + job.skills.map(function(skill){return "'" + eq(skill) + "'";}).join(",") + "];" : "") +
                    (job.postedDate ? "ctx._source.jobPostingDate='" + job.postedDate + "';" : "") +
                    (job.salaryCurrency && job.salaryCurrency.length > 0 ? "ctx._source.jobSalaryCurrency='" + job.salaryCurrency + "';" : "") +
                    (job.minimumSalary ? "ctx._source.jobMinimumSalary='" + job.minimumSalary + "';" : "") +
                    (job.maximumSalary ? "ctx._source.jobMaximumSalary=" + job.maximumSalary + ";" : "") +
                    (job.salaryTimeSpan && job.salaryTimeSpan.length > 0 ? "ctx._source.jobSalaryTimeSpan='" + job.salaryTimeSpan + "';" : "") +
                    ((job.team && job.team.id ? "ctx._source['jobPostedByTeam']=teamObject" : "")),
                "params": {
                    "teamObject": job.team
                },
            }
        };
        //console.log("Updating with jobid " + job.id);
        //console.log(JSON.stringify(query));
        sendData(query, utilities.getCandidateIndex() + "/jobcandidateinteraction/_update_by_query", "POST").then(function (response) {
            //console.log("Response = " + JSON.stringify(response));
            nextJob();
        }).catch(function (err) {
            utilities.writeJSONToFile(query);
            console.log(err);
            nextJob();
        });
    }

    function tagWorkAssignmentInformationToCandidate() {
        context.workassignemntIndex = 0;
        if (context.job.assignedTo && context.job.assignedTo.length > 0) {
            updateJobInformationToCandidate();
        } else {
            nextJob();
        }
    }

    function updateJobInformationToCandidate() {
        var job = context.job;
        var wa = job.assignedTo[context.workassignemntIndex];
        if (wa) {
            var query = {
                "query": {
                    "bool": {
                        "must": [{
                            "term": {
                                "jobId": job.id
                            }
                        }, {
                            "term": {
                                "recruiterId.verbatim": wa.recruiterId
                            }
                        }]
                    }
                },
                "script": {
                    "inline": "ctx._source['jobWorkAssignment']=waObject;",
                    "params": {
                        "waObject": wa
                    },
                }
            };
            console.log("Job = " + job.id + " recruiterId = " + wa.recruiterId);
            console.log(JSON.stringify(query));
            sendData(query, utilities.getCandidateIndex() + "/jobcandidateinteraction/_update_by_query", "POST").then(function (response) {
                console.log("Response = " + JSON.stringify(response));
                context.workassignemntIndex = context.workassignemntIndex + 1;
                updateJobInformationToCandidate();
            }).catch(function (err) {
                console.log(err);
            });
        } else {
            nextJob();
        }

    }

    function eq(r) {
        return r.replace(/'/g, "''");
    }

    setTimeout(start, 2000);

}
