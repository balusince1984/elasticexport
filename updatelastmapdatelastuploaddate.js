var moment = require('moment');
const fs = require('fs');

module.exports = function (app, utilities, env) {

    const logger = utilities.logger;
    const sendData = utilities.sendData;
    const getData = utilities.getData;
    var count = 0;
    var skip = -1;


    var context = {
        _scroll_id: null,
        mappingIndex: -1,
        response: null
    }

    function start() {
        logger.info("Process Started");
        context.running = 1;
        context.index = 0;
        context.stop = 0;
        createScrollToken();
    }

    function createScrollToken() {
        sendData({
            "size": 5000,
            "query": {
                "match_all": {}
            }
        }, utilities.getCandidateIndex() + "candidateaggregator/candidateaggregator/_search?scroll=1m", "POST", {
                scroll: "1m"
            }).then(function (response) {
                context.response = response;
                context._scroll_id = response._scroll_id;
                nextRecord();
            }).catch(function (err) {
                logger.error(err);
            });
    }

    function processESData() {
        logger.info("Parsed records %s", count);
        sendData({
            "scroll": "1m",
            "scroll_id": context._scroll_id
        }, utilities.getCandidateIndex() + "/_search/scroll", "POST").then(function (response) {
            try {
                context.response = response;
                context.mappingIndex = -1;
                context._scroll_id = response._scroll_id;
                if (context.response.hits.hits.length > 0) {
                    nextRecord();
                } else {
                    logger.info("Exit the process");
                }
               
            } catch (err) {
                logger.error(err);
            }
        }).catch(function (err) {
            logger.error(err);
        });
    }

    function syncCandidateMappings() {
        var cand = context.response.hits.hits[context.mappingIndex]["_source"];
        cand.lastMappingDate = getLastDate(cand.mapping);
        cand.lastUploadDate = getLastDate(cand.cv);
        cand.firstMappingDate = getFirstDate(cand.mapping);
        cand.firstUploadDate = getFirstDate(cand.cv);
        sendData(cand, utilities.getCandidateIndex() + "/candidateaggregator/candidateaggregator/" + cand.id, "POST").then(function (response) {
            nextRecord();
        }).catch(function (err) {
            logger.error(err);
            nextRecord();
        });
    }

    function nextRecord() {
        try {
            context.mappingIndex = context.mappingIndex + 1;
            if (context.response.hits.hits[context.mappingIndex]) {
                syncCandidateMappings();
            } else {
                processESData();
            }
        } catch (err) {
            logger.error(err);
        }
    }

    function getLastDate(dates) {
        var lastDate = null;
        for (d in dates) {
            if (lastDate == null) {
                lastDate = dates[d].createdOn;
                continue;
            }
            if (!moment(dates[d].createdOn).isBefore(lastDate)) {
                lastDate = dates[d].createdOn;
            }
        }
  //      if(dates.length > 1)
  //          logger.info("Last date among %j is %s", dates, lastDate);
        return lastDate;
    }

    function getFirstDate(dates) {
        var firstDate = null;
        for (d in dates) {
            if (firstDate == null) {
                firstDate = dates[d].createdOn;
                continue;
            }
            if (moment(dates[d].createdOn).isBefore(firstDate)) {
                firstDate = dates[d].createdOn;
            }
        }
//        if(dates.length > 1)
 //           logger.info("First date among %j is %s", dates, firstDate);
        return firstDate;
    }

    setTimeout(start, 2000);

}
