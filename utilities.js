const winston = require('winston');
const tsFormat = () => (new Date()).toLocaleTimeString();
const fs = require('fs');
const rp = require('request-promise');
const http = require('http');
var env;
var logger = null;

module.exports = {

    logger: new (winston.Logger)({
        transports: [
            new (winston.transports.Console)({
                timestamp: tsFormat,
                colorize: true,
                level: 'info'
            }),
            new (winston.transports.File)({
                filename: `log${(new Date()).getTime()}.log`,
                timestamp: tsFormat,
                level: 'debug'
            })
        ]
    }),


    init: function (e) {
        env = e;
    },


    getJobIndex: function () {
        if ('development' == env) {
            return "http://localhost:9202/";
        }
        return "http://localhost:9200/";
    },

    getCandidateIndex: function () {
        if ('development' == env) {
            return "http://localhost:9200/";
        }
        return "http://localhost:9200/";
    },

    writeJSONToFile: function (obj) {
        fs.writeFileSync('failed-' + (new Date()).getTime() + '.json', JSON.stringify(obj, null, 2), 'utf-8');
    },

    sendData: function (obj, command, method, qs) {
        var options = {
            method: method,
            uri: command,
            body: obj ? obj : {},
            headers: {
                'content-type': 'application/json; charset=UTF-8'
            },
            qs: qs,
            json: true
        };
        return rp(options);
    },

    getData: function (command, qs) {
        var options = {
            method: "GET",
            uri: command,
            headers: {
                'content-type': 'application/json; charset=UTF-8'
            },
            qs: qs,
            json: true
        };
        return rp(options);
    },
};
